<a name="readme-top"></a>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

### Built With

- [![React][react.js]][react-url] ^17.0.2
- [![Nodejs][node.js]][node-url] ^14.18.1
- [![Typescript][typescript.ts]][typescript-url] ^4.4.4

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

Running follow these simple example steps to setup.

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Install NPM packages
   ```sh
   npm install
   ```
2. Running project
   ```sh
   npm start
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

Project structure:

    # src
        # api - Contain function call api
        # assets - Contain assets storage
        # components - Contain common components
        # container - Define all components use for routes folder
        # routes - Contain all page define in route
        # themes - Define theme for project
        # define - Define interface object for project
        # redux - Contain all redux
        index.tsx - Place where application start

My app has 3 page: - [My page](http://localhost:3000/) - [My record](http://localhost:3000/myrecord) - [Column](http://localhost:3000/columnpage)

Problem:

    - I use redux for storing & access data
    - I assume data is fetch from api (location in api folder)
    - Concerned with structure project's folder (don't know if it can scale or not)
    - Data, view, style are totally separate. Easily to maintain & develop
    - I also add protected route, which redirect to Column page if not authentication
    - Login/logout: click a button in menu navbar (ログイン / ログアウト)
    - About font style: because require 2 font for 2 text (japan & other). Only 1 font can be set to body so the other need to specify in style

<p align="right">(<a href="#readme-top">back to top</a>)</p>

[product-screenshot]: src/assets/images/screenshot.png
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[react-url]: https://reactjs.org/
[node.js]: https://img.shields.io/badge/node.js-000000?style=for-the-badge&logo=nodedotjs&logoColor=white
[node-url]: https://nodejs.org/en/
[typescript.ts]: https://img.shields.io/badge/Typescript-0769AD?style=for-the-badge&logo=typescript&logoColor=white
[typescript-url]: https://www.typescriptlang.org/
