/**
 * Base Colors
 *
 * Others
 */

const colors = {
  // BaseColors
  light: "#fff",
  primary: "#FFCC21",
  iconPrimary: "#FF963C",
  notifyPrimary: "#EA6C00",
  secondary: "#8FE9D0",
  dark: "#2E2E2E",
  mainDark: "#414141",
  gray: "#777777",
};

export default colors;
