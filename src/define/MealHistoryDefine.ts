export interface IMealHistoryItem {
  image: string;
  mealInfo: string;
}
