export interface IDiary {
  time: number;
  title: string;
  text: string;
}
export interface IDataDiary {
  diaries: IDiary[];
}
