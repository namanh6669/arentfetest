export interface IExerciseRecord {
  title: string;
  volume: number;
  volumeUnit: string;
  duration: number;
  durationUnit: string;
}
export interface IDataExerciseRecord {
  date: string;
  exercises: IExerciseRecord[];
}
