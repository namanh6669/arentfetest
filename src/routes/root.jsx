import Footer from "components/Footer/Footer";
import Header from "components/Header/Header";
import { Outlet } from "react-router-dom";

export default function Root() {
  return (
    <>
      <Header></Header>
      <div id="detail" style={{flex: 1}}>
        <Outlet />
      </div>
      <Footer></Footer>
    </>
  );
}