import {
  BodyFatPercentGraph,
  Diaries,
  ExerciseRecord,
  MenuScreen,
} from "containers/MyRecordContainer";
import "./styles.scss";
export default function MyRecord() {
  return (
    <div className="myrecord-page">
      <MenuScreen />
      <div className="break-div"></div>
      <BodyFatPercentGraph />
      <div className="break-div"></div>
      <ExerciseRecord />
      <div className="break-div"></div>
      <Diaries />
    </div>
  );
}
