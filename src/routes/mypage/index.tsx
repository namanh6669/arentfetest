import { BodyFatLineChart } from "components/HighChart";
import {
  AchievementRate,
  ButtonList,
  MealHistory,
} from "containers/MyPageContainer";
import "./styles.scss";

export default function MyPage() {
  return (
    <div className="mypage-root">
      <div className="achievement-wrapper">
        <AchievementRate />
        <div style={{ flex: "1 1 60%" }}>
          <BodyFatLineChart />
        </div>
      </div>
      <div className="meal-body">
        <ButtonList />
        <MealHistory />
      </div>
    </div>
  );
}
