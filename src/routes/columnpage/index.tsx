import { MenuScreen, RecommendRecord } from "containers/ColumnPageContainer";
import "./styles.scss";

export default function ColumnPage() {
  return (
    <div className="columnrecord-page">
      <MenuScreen />
      <div className="break-div"></div>
      <RecommendRecord />
    </div>
  );
}
