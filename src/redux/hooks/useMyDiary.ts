import { useDispatch, useSelector } from "react-redux";

import * as actionType from "redux/actions";

const useMyDiary = () => {
  const dispatch = useDispatch();
  const myDiaries = useSelector((state: any) => state.getMyDiary).data;

  const getMyDiary = () => {
    dispatch(actionType.getMyDiary());
  };

  return {
    myDiaries,
    getMyDiary,
  };
};

export { useMyDiary };
