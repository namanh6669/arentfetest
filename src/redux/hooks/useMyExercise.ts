import { useDispatch, useSelector } from "react-redux";

import * as actionType from "redux/actions";

const useMyExercise = () => {
  const dispatch = useDispatch();
  const myExercises = useSelector((state: any) => state.getMyExercise).data;

  const getMyExercise = () => {
    dispatch(actionType.getMyExercise());
  };

  return {
    myExercises,
    getMyExercise,
  };
};

export { useMyExercise };
