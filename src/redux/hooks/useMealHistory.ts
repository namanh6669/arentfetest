import { useDispatch, useSelector } from "react-redux";

import * as actionType from "redux/actions";

const useMealHistory = () => {
  const dispatch = useDispatch();
  const mealHistories = useSelector((state: any) => state.getMealHistory).data;

  const getMealHistory = () => {
    dispatch(actionType.getMealHistory());
  };

  return {
    mealHistories,
    getMealHistory,
  };
};

export { useMealHistory };
