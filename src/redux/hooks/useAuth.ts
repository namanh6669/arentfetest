import { useDispatch, useSelector } from "react-redux";

import * as actionType from "redux/actions";

interface IAuthLoginParams {
  onSuccess: () => void;
}
const useAuth = () => {
  const dispatch = useDispatch();
  const token = useSelector((state: any) => state.auth).token;

  const postAuthLogin = (params: IAuthLoginParams) => {
    dispatch(actionType.postAuthLogin(params));
  };
  const postAuthLogout = () => {
    dispatch(actionType.postAuthLogout());
  };

  return {
    token,
    postAuthLogin,
    postAuthLogout,
  };
};

export { useAuth };
