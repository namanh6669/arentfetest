import {
  GET_MEAL_HISTORY,
  GET_MY_DIARY,
  GET_MY_EXERCISE,
  REQUEST_AUTH_LOGIN,
  AUTH_LOGOUT,
} from "./actionTypes";

export const postAuthLogin = (payload: any) => {
  return {
    type: REQUEST_AUTH_LOGIN,
    payload,
  };
};
export const postAuthLogout = () => {
  return {
    type: AUTH_LOGOUT,
  };
};
export const getMyExercise = () => {
  return {
    type: GET_MY_EXERCISE,
  };
};
export const getMyDiary = () => {
  return {
    type: GET_MY_DIARY,
  };
};
export const getMealHistory = () => {
  return {
    type: GET_MEAL_HISTORY,
  };
};
