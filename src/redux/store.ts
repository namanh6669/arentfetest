import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers/index";

import {
  getMyExerciseSaga,
  getMyDiarySaga,
  getMealHistorySaga,
  handleAuthSaga,
} from "./sagas";

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(getMyExerciseSaga);
sagaMiddleware.run(getMyDiarySaga);
sagaMiddleware.run(getMealHistorySaga);
sagaMiddleware.run(handleAuthSaga);
