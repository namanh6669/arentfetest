import { put, call, takeEvery } from "redux-saga/effects";
import * as actionType from "redux/actions/actionTypes";
import { getMealHistories } from "api/mypage";

export default function* getMealHistorySaga() {
  yield takeEvery(actionType.GET_MEAL_HISTORY, fetchMealHistory);
}

function* fetchMealHistory(): Generator<any> {
  try {
    const dataResponse = yield call(getMealHistories);
    yield put({ type: actionType.GOT_MEAL_HISTORY, payload: dataResponse });
  } catch (err) {
    console.log(err);
  }
}
