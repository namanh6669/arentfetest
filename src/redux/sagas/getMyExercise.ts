import { put, call, takeEvery } from "redux-saga/effects";
import * as actionType from "redux/actions/actionTypes";
import { getMyExercise } from "api/myrecord";

export default function* getMyExerciseSaga() {
  yield takeEvery(actionType.GET_MY_EXERCISE, fetchMyExercise);
}

function* fetchMyExercise(): Generator<any> {
  try {
    const dataResponse = yield call(getMyExercise);
    yield put({ type: actionType.GOT_MY_EXERCISE, payload: dataResponse });
  } catch (err) {
    console.log(err);
  }
}
