import { put, call, takeEvery } from "redux-saga/effects";
import * as actionType from "redux/actions/actionTypes";
import { login } from "api/auth";

export default function* handleAuthSaga() {
  yield takeEvery(actionType.REQUEST_AUTH_LOGIN, fetchAuthLogin);
}

function* fetchAuthLogin(payload: any): Generator<any> {
  try {
    const {
      payload: { onSuccess },
    } = payload;
    const dataResponse = yield call(login);
    yield put({ type: actionType.RESPONSE_AUTH_LOGIN, payload: dataResponse });
    onSuccess();
  } catch (err) {
    console.log(err);
  }
}
