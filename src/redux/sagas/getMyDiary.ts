import { put, call, takeEvery } from "redux-saga/effects";
import * as actionType from "redux/actions/actionTypes";
import { getMyDiary } from "api/myrecord";

export default function* getMyDiarySaga() {
  yield takeEvery(actionType.GET_MY_EXERCISE, fetchMyDiary);
}

function* fetchMyDiary(): Generator<any> {
  try {
    const dataResponse = yield call(getMyDiary);
    yield put({ type: actionType.GOT_MY_DIARY, payload: dataResponse });
  } catch (err) {
    console.log(err);
  }
}
