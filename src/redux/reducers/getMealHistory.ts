import * as actionType from "redux/actions/actionTypes";

const initialState = {
  data: [],
};

export default function getMealHistory(state = initialState, action: any) {
  switch (action.type) {
    case actionType.GOT_MEAL_HISTORY: {
      return {
        ...state,
        data: action.payload,
      };
    }
    default: {
      return { ...state };
    }
  }
}
