import * as actionType from "redux/actions/actionTypes";

const initialState = {
  token: "",
};

export default function handleAuth(state = initialState, action: any) {
  switch (action.type) {
    case actionType.RESPONSE_AUTH_LOGIN: {
      return {
        ...state,
        token: action.payload,
      };
    }
    case actionType.AUTH_LOGOUT: {
      return {
        ...state,
        token: "",
      };
    }
    default: {
      return { ...state };
    }
  }
}
