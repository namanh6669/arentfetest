import { combineReducers } from "redux";

import getMyExercise from "./getMyExercise";
import getMyDiary from "./getMyDiary";
import getMealHistory from "./getMealHistory";
import handleAuth from "./handleAuth";

export default combineReducers({
  getMyExercise,
  getMyDiary,
  getMealHistory,
  auth: handleAuth,
});
