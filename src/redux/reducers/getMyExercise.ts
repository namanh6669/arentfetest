import * as actionType from "redux/actions/actionTypes";

const initialState = {
  data: [],
};

export default function getMyExercise(state = initialState, action: any) {
  switch (action.type) {
    case actionType.GOT_MY_EXERCISE: {
      return {
        ...state,
        data: action.payload,
      };
    }
    default: {
      return { ...state };
    }
  }
}
