import React from "react";

const menus = [
  {
    title: "RECOMMENDED COLUMN",
    subTitle: "オススメ",
    href: "",
  },
  {
    title: "RECOMMENDED DIET",
    subTitle: "ダイエット",
    href: "",
  },
  {
    title: "RECOMMENDED BEAUTY",
    subTitle: "美容",
    href: "",
  },
  {
    title: "RECOMMENDED HEALTH",
    subTitle: "健康",
    href: "",
  },
];
interface MenuScreenItemProps {
  title: string;
  subTitle: string;
  href?: string;
}

const MenuScreenItem: React.FC<MenuScreenItemProps> = (props) => {
  return (
    <a className="item-wapper" href={props.href || ""}>
      <div className="item">
        <div className="content">
          <span>{props.title}</span>
          <span>{props.subTitle}</span>
        </div>
      </div>
    </a>
  );
};

export default function MenuScreen() {
  return (
    <div className="MenuScreen">
      {menus.map((item) => (
        <MenuScreenItem key={item.title} {...item} />
      ))}
    </div>
  );
}
