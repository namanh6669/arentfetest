import { Meal } from "components/Meal";
import { PrimaryButton } from "components/Button";

export default function MealHistory() {
  const data = [
    {
      image: "/images/column-1.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-2.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-3.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-4.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-5.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-6.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-7.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
    {
      image: "/images/column-8.jpg",
      mealInfo: "2021.05.17   23:25",
      mealDescription:
        "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ,魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメ",
      hashtags: ["魚料理", "和食", "DHA"],
    },
  ];
  return (
    <>
      <div className="recommend-list-wrapper">
        {data.length > 0 &&
          data.map((x, index) => {
            return (
              <Meal
                image={x.image}
                mealInfo={x.mealInfo}
                mealDescription={x.mealDescription}
                hashtags={x.hashtags}
                key={index.toString()}
              />
            );
          })}
      </div>
      <div className="loadmore">
        <PrimaryButton text="コラムをもっと見る"></PrimaryButton>
      </div>
    </>
  );
}
