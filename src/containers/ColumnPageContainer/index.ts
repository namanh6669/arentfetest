import MenuScreen from "./MenuScreen";
import RecommendRecord from "./RecommendRecord";
import "./styles.scss";

export { MenuScreen, RecommendRecord };
