import React from "react";

const menus = [
  {
    title: "BODY RECORD",
    subTitle: "自分のカラダの記録",
    thumb: "/images/MyRecommend-1.jpg",
    href: "#BodyFatLineChart",
  },
  {
    title: "MY EXERCISE",
    subTitle: "自分の運動の記録",
    thumb: "/images/MyRecommend-2.jpg",
    href: "#ExerciseRecord",
  },
  {
    title: "MY DIARY",
    subTitle: "自分の日記",
    thumb: "/images/MyRecommend-3.jpg",
    href: "#Diaries",
  },
];
interface MenuScreenItemProps {
  title: string;
  subTitle: string;
  thumb: string;
  href?: string;
}

const MenuScreenItem: React.FC<MenuScreenItemProps> = (props) => {
  return (
    <a className="item-wapper" href={props.href || ""}>
      <div className="item" style={{ backgroundImage: `url(${props.thumb})` }}>
        <div className="content">
          <span>{props.title}</span>
          <span>{props.subTitle}</span>
        </div>
      </div>
    </a>
  );
};

export default function MenuScreen() {
  return (
    <div className="MenuScreen">
      {menus.map((item) => (
        <MenuScreenItem key={item.title} {...item} />
      ))}
    </div>
  );
}
