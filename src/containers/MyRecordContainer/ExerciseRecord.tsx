import { IExerciseRecord } from "define/ExerciseRecordDefine";
import React from "react";
import { useMyExercise } from "redux/hooks/useMyExercise";

const ExerciseRecordItem: React.FC<IExerciseRecord> = (props) => {
  return (
    <div className="ex-item-wapper">
      <span>.</span>
      <div className="ex-item-left">
        <span>{props.title}</span>
        <span>{`${props.volume}${props.volumeUnit}`}</span>
      </div>
      <span className="ex-item-right">{`${props.duration} ${props.durationUnit}`}</span>
    </div>
  );
};

export default function ExerciseRecord() {
  const { getMyExercise, myExercises } = useMyExercise();
  React.useLayoutEffect(() => {
    getMyExercise();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="ExerciseRecord">
      <div className="ex-header">
        <span>
          MY
          <br />
          EXERCISE
        </span>
        <span>{myExercises.date}</span>
      </div>
      <div id="ExerciseRecord" className="ex-content">
        {myExercises?.exercises?.map((item: IExerciseRecord, index: number) => (
          <ExerciseRecordItem key={`${item.title}${index}`} {...item} />
        ))}
      </div>
    </div>
  );
}
