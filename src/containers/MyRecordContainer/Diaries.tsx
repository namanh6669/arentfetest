import { PrimaryButton } from "components/Button";
import { IDiary } from "define/MyDiaryDefine";
import moment from "moment";
import React from "react";
import { useMyDiary } from "redux/hooks/useMyDiary";

const DiaryItem: React.FC<IDiary> = (props) => {
  return (
    <div className="diary-item-wapper">
      <div className="diary-item-content">
        <span className="diary-item-time">
          {moment(props.time).format("YYYY.MM.DD")}
        </span>
        <span className="diary-item-time">
          {moment(props.time).format("hh:mm")}
        </span>
        <span className="diary-item-title">{props.title}</span>
        <span className="diary-item-text">{props.text}</span>
      </div>
    </div>
  );
};

export default function Diaries() {
  const { getMyDiary, myDiaries } = useMyDiary();
  React.useLayoutEffect(() => {
    getMyDiary();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div id="Diaries" className="Diaries">
      <div className="diary-header">
        <span>MY DIARY</span>
      </div>
      <div className="diary-content">
        {myDiaries?.diaries?.map((item: IDiary, index: number) => (
          <DiaryItem key={`${item.title}${index}`} {...item} />
        ))}
      </div>
      <div className="loadmore">
        <PrimaryButton text="自分の日記をもっと見る"></PrimaryButton>
      </div>
    </div>
  );
}
