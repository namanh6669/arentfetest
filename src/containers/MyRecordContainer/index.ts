import BodyFatPercentGraph from "./BodyFatPercentGraph";
import Diaries from "./Diaries";
import ExerciseRecord from "./ExerciseRecord";
import MenuScreen from "./MenuScreen";
import "./styles.scss";

export { BodyFatPercentGraph, Diaries, ExerciseRecord, MenuScreen };
