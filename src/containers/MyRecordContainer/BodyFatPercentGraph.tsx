import { BodyFatLineChart } from "components/HighChart";

export default function BodyFatPercentGraph() {
  return (
    <div id="BodyFatLineChart">
      <BodyFatLineChart />
    </div>
  );
}
