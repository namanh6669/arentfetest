import { Button } from "components/Button";
import { ReactComponent as Fork } from "assets/images/fork.svg";
import { ReactComponent as Coffee } from "assets/images/coffee-15.svg";

interface ButtonListProps {}
const ButtonList: React.FC<ButtonListProps> = () => {
  return (
    <div className="ButtonList">
      <Button icon={<Fork />} text={"Morning"} />
      <Button icon={<Fork />} text={"Launch"} />
      <Button icon={<Fork />} text={"Dinner"} />
      <Button icon={<Coffee />} text={"Snack"} />
    </div>
  );
};
export default ButtonList;
