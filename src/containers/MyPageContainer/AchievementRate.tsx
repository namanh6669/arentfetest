import background from "assets/images/achievement_bg.png";
import { ProgressBar } from "components/ProgressBar";

interface AchievementRateProps {}
const AchievementRate: React.FC<AchievementRateProps> = () => {
  return (
    <div
      className="AchievementRate"
      style={{ backgroundImage: `url(${background})` }}
    >
      <div className="achievement-progress">
        <ProgressBar current={3} total={4}></ProgressBar>
      </div>
    </div>
  );
};
export default AchievementRate;
