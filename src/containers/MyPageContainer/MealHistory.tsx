import { Meal } from "components/Meal";
import { PrimaryButton } from "components/Button";
import { useMealHistory } from "redux/hooks/useMealHistory";
import React from "react";
import { IMealHistoryItem } from "define/MealHistoryDefine";

export default function MealHistory() {
  const { getMealHistory, mealHistories } = useMealHistory();
  React.useLayoutEffect(() => {
    getMealHistory();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div className="meal-list-wrapper">
        {mealHistories?.length > 0 &&
          mealHistories?.map((x: IMealHistoryItem, index: number) => {
            return (
              <Meal
                image={x.image}
                mealInfo={x.mealInfo}
                key={index.toString()}
              />
            );
          })}
      </div>
      <div className="loadmore">
        <PrimaryButton text="記録をもっと見る"></PrimaryButton>
      </div>
    </>
  );
}
