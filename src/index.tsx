import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root from "./routes/root";
import MyRecord from "routes/myrecord";
import MyPage from "routes/mypage";
import ColumnPage from "routes/columnpage";
import { ProtectedRoute } from "components/ProtectedRoute/ProtectedRoute";

import { Provider } from "react-redux";
import { store } from "redux/store";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root></Root>,
    children: [
      {
        path: "",
        element: <ProtectedRoute></ProtectedRoute>,
        children: [
          {
            path: "myrecord",
            element: <MyRecord />,
          },
          {
            path: "",
            element: <MyPage />,
          },
        ],
      },
      {
        path: "columnpage",
        element: <ColumnPage />,
      },
    ],
  },
]);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
