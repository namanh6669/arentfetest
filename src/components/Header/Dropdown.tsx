import { Link } from "react-router-dom";
import "./styles.scss";
import { useAuth } from "redux/hooks/useAuth";
import { useNavigate } from "react-router-dom";

const Dropdown = () => {
  const { postAuthLogin, postAuthLogout, token } = useAuth();
  const navigate = useNavigate();

  const loginSuccess = () => {
    navigate("/");
  };
  return (
    <ul className="dropdown">
      {token ? (
        <li
          onClick={() => {
            postAuthLogout();
            navigate("/");
          }}
          className="menu-items"
        >
          <span className="text">ログアウト</span>
        </li>
      ) : (
        <li
          onClick={() => {
            postAuthLogin({
              onSuccess: loginSuccess,
            });
          }}
          className="menu-items"
        >
          <span className="text">ログイン</span>
        </li>
      )}
      <li className="menu-items">
        <Link to={""} className="text">
          自分の記録
        </Link>
      </li>
      <li className="menu-items">
        <Link to={""} className="text">
          体重グラフ
        </Link>
      </li>
      <li className="menu-items">
        <Link to={""} className="text">
          目標
        </Link>
      </li>
      <li className="menu-items">
        <Link to={""} className="text">
          選択中のコース
        </Link>
      </li>
      <li className="menu-items">
        <Link to={""} className="text">
          コラム一覧
        </Link>
      </li>
      <li className="menu-items">
        <Link to={""} className="text">
          設定
        </Link>
      </li>
    </ul>
  );
};

export default Dropdown;
