import "./styles.scss";
import { ReactComponent as ReactLogo } from "assets/images/logo.svg";
import { ReactComponent as IconNote } from "assets/images/icon_note.svg";
import { ReactComponent as IconTop } from "assets/images/icon_top.svg";
import { ReactComponent as IconNoti } from "assets/images/icon_noti.svg";
import { ReactComponent as IconMenu } from "assets/images/icon_menu.svg";
import { ReactComponent as IconMenuClose } from "assets/images/icon_close.svg";
import { Link, useLocation } from "react-router-dom";
import Dropdown from "./Dropdown";
import { useState } from "react";

const Header = () => {
  const [showMenu, setShowMenu] = useState(false);
  const location = useLocation();

  const toggleMenu = () => {
    setShowMenu(!showMenu);
  };

  return (
    <header>
      <nav className="nav-wrapper">
        <div className="logo-wrapper">
          <ReactLogo />
        </div>
        <ul className="menu-content">
          <li>
            <Link className="link" to={"/myrecord"}>
              <IconNote />{" "}
              <span
                className={location.pathname === "/myrecord" ? "active" : ""}
              >
                自分の記録
              </span>
            </Link>
          </li>
          <li>
            <Link className="link" to={""}>
              <IconTop /> <span>チャレンジ</span>
            </Link>
          </li>
          <li>
            <Link className="link" to={""}>
              <IconNoti /> <span>お知らせ</span>
            </Link>
          </li>
          <li style={{ position: "relative" }}>
            {showMenu ? (
              <IconMenuClose
                className="link"
                style={{ cursor: "pointer" }}
                onClick={() => toggleMenu()}
              />
            ) : (
              <IconMenu
                className="link"
                style={{ cursor: "pointer" }}
                onClick={() => toggleMenu()}
              />
            )}
            {showMenu && <Dropdown />}
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
