import './styles.scss'

interface ProgressBarProps {
    size?: number;
    trackWidth?: number;
    trackColor?: string;
    indicatorWidth?: number;
    indicatorColor?: string;
    indicatorCap?: "round" | "inherit" | "butt" | "square" | undefined;
    labelColor?: string;
    spinnerMode?: boolean;
    current?: number;
    total?: number
}

const ProgressBar: React.FC<ProgressBarProps> = ({
    size = 181,
    trackWidth = 3,
    trackColor = undefined,
    indicatorWidth = 4,
    indicatorColor = `#fff`,
    indicatorCap = `round`,
    labelColor = `#fff`,
    spinnerMode = false,
    current = 3,
    total = 4,
  }) => {

  const progress = (current / total) * 100;

  const center = size / 2,
        radius = center - (trackWidth > indicatorWidth ? trackWidth : indicatorWidth),
        dashArray = 2 * Math.PI * radius,
        dashOffset = dashArray * ((100 - progress) / 100)

  let hideLabel = false;


  return (
    <>
      <div
        className="svg-pi-wrapper"
        style={{ width: size, height: size }}
      >
        <svg
          className="svg-pi" 
          style={{ width: size, height: size }}
        >
          <circle
            className="svg-pi-track"
            cx={center}
            cy={center}
            fill="transparent"
            r={radius}
            stroke={trackColor}
            strokeWidth={trackWidth}
          />
          <circle
            className={`svg-pi-indicator ${
              spinnerMode ? "svg-pi-indicator--spinner" : ""
            }`}
            cx={center}
            cy={center}
            fill="transparent"
            r={radius}
            stroke={indicatorColor}
            strokeWidth={indicatorWidth}
            strokeDasharray={dashArray}
            strokeDashoffset={dashOffset}
            strokeLinecap={indicatorCap}
            
          />
        </svg>

        {!hideLabel && (
          <div 
            className="svg-pi-label" 
            style={{ color: labelColor }}
          >
            {!spinnerMode && (
                <>
                    <span className="svg-pi-label__progress">{current}/{total}</span>
                    <span className="svg-pi-label__progress percentage">
                        {`${
                        progress > 100 ? 100 : progress
                        }%`}
                    </span>
                </>
              
            )}
          </div>
        )}
      </div>
    </>
  )
}

export default ProgressBar