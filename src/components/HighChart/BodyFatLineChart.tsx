import * as React from "react";
import * as Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import colors from "themes/color";
import BodyFatData from "components/HighChart/BodyFatData.json";
import BodyFatDataSecondary from "components/HighChart/BodyFatDataSecondary.json";

const options = (
  chartData: any,
  chartDataSecondary: any
): Highcharts.Options => ({
  chart: {
    backgroundColor: colors.mainDark,
  },
  title: {
    align: "left",
    text: "BODY<br/>RECORD",
    style: {
      fontSize: "15",
      fontWeight: "400",
      color: colors.light,
    },
  },
  subtitle: {
    text: "2021.05.21",
    style: {
      color: colors.light,
      fontWeight: "400",
      fontSize: "22",
    },
    align: "left",
    verticalAlign: "top",
    x: 100,
    y: 20,
  },
  scrollbar: {
    enabled: false,
  },
  xAxis: {
    type: "datetime",
    labels: {
      style: {
        color: colors.light,
        fontSize: "14",
        fontWeight: "300",
      },
    },
    lineWidth: 0,
    gridLineWidth: 1,
    dateTimeLabelFormats: {
      // do display the year
      month: "%b",
      year: "%Y",
    },
  },
  yAxis: {
    visible: false,
    gridLineWidth: 0,
  },
  plotOptions: {
    series: {
      lineWidth: 3,
    },
  },
  legend: {
    enabled: false,
  },
  rangeSelector: {
    labelStyle: {
      display: "none",
    },
    verticalAlign: "bottom",
    allButtonsEnabled: true,
    inputEnabled: false,
    buttonTheme: {
      width: 60,
      // styles for the buttons
      fill: colors.light,
      stroke: "none",
      "stroke-width": 0,
      r: 11,
      style: {
        color: colors.primary,
        fontWeight: "300",
        fontSize: "15",
      },
      states: {
        hover: {
          fill: colors.primary,
          style: {
            color: colors.light,
          },
        },
        select: {
          fill: colors.primary,
          style: {
            color: colors.light,
          },
        },
        // disabled: { ... }
      },
    },
    buttons: [
      {
        type: "day",
        count: 3,
        text: "日",
        dataGrouping: {
          forced: true,
          units: [["hour", [1]]],
        },
      },
      {
        type: "week",
        count: 1,
        text: "週",
        dataGrouping: {
          forced: true,
          units: [["day", [1]]],
        },
      },
      {
        type: "month",
        text: "月",
        dataGrouping: {
          forced: true,
          units: [["week", [1]]],
        },
      },
      {
        type: "year",
        text: "年",
        dataGrouping: {
          forced: true,
          units: [["month", [1]]],
        },
      },
    ],
    selected: 3,
  },
  navigator: {
    enabled: false,
  },
  colors: [colors.primary, colors.secondary],
  series: [
    {
      type: "line",
      data: chartData,
    },
    {
      type: "line",
      data: chartDataSecondary,
    },
  ],
});

const BodyFatLineChart = (props: HighchartsReact.Props) => {
  const chartComponentRef = React.useRef<HighchartsReact.RefObject>(null);
  const [chartData] = React.useState(BodyFatData);
  const [chartDataSecondary] = React.useState(BodyFatDataSecondary);
  React.useEffect(() => {
    Highcharts.setOptions({
      lang: {
        months: [
          "1月",
          "2月",
          "3月",
          "4月",
          "5月",
          "6月",
          "7月",
          "8月",
          "9月",
          "10月",
          "11月",
          "12月",
        ],
        shortMonths: [
          "1月",
          "2月",
          "3月",
          "4月",
          "5月",
          "6月",
          "7月",
          "8月",
          "9月",
          "10月",
          "11月",
          "12月",
        ],
      },
    });
    chartComponentRef.current?.chart.redraw();
  }, [chartComponentRef]);

  return (
    <HighchartsReact
      constructorType={"stockChart"}
      highcharts={Highcharts}
      options={options(chartData, chartDataSecondary)}
      ref={chartComponentRef}
      {...props}
    />
  );
};
export default BodyFatLineChart;
