interface IPrimaryButtonPpops {
  text: string;
  onClick?: () => void;
}

export default function PrimaryButton(props: IPrimaryButtonPpops) {
  return <span className="button-component">{props.text}</span>;
}
