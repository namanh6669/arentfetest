import * as React from "react";
import { ReactComponent as ButtonSvg } from "assets/images/button.svg";

interface ButtonProps {
  icon: React.ReactElement;
  text: string;
}

const Button: React.FC<ButtonProps> = ({ icon, text }) => {
  return (
    <div className="button-wrapper">
      <ButtonSvg />
      <div className="content">
        <div>{icon}</div>
        <div>{text}</div>
      </div>
    </div>
  );
};
export default Button;
