/* eslint-disable jsx-a11y/anchor-is-valid */
import * as React from "react";
import "./styles.scss";

interface MealProps {
  image?: string;
  mealInfo: string;
  mealDescription?: string;
  hashtags?: string[];
}

const Meal: React.FC<MealProps> = ({
  image,
  mealInfo,
  mealDescription,
  hashtags,
}) => {
  return (
    <div className="Meal">
      <div className="child">
        <div className="meal-wrapper">
          <img src={image} alt="" />
          <div className="meal-tag">
            <div>{mealInfo}</div>
          </div>
        </div>
      </div>
      {mealDescription || hashtags ? (
        <div className="meal-info">
          <span className="meal-description">{mealDescription || ""}</span>
          <div className="meal-hashtags">
            {hashtags?.map((tag, index) => (
              <a key={`${tag}${index}`} href="">
                <span className="meal-hashtag">#{tag || ""}</span>
              </a>
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
};
export default Meal;
