const userToken = "MY_TOKEN_AFTER_LOGIN";
const login = async () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(userToken), 1);
  });
};

export { login };
