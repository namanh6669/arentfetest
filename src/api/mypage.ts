import { IMealHistoryItem } from "define/MealHistoryDefine";

const mealHistories: IMealHistoryItem[] = [
  { image: "/images/m01.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/l03.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/d01.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/l01.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/l02.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/d02.jpg", mealInfo: "05.21.Morning" },
  { image: "/images/s01.jpg", mealInfo: "05.21.Morning" },
];

const getMealHistories = async () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(mealHistories), 2000);
  });
};

export { getMealHistories };
