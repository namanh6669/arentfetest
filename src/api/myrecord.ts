import { IDataExerciseRecord } from "define/ExerciseRecordDefine";
import { IDataDiary } from "define/MyDiaryDefine";

const MyExercises: IDataExerciseRecord = {
  date: "2021.05.21",
  exercises: [
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
    {
      title: "家事全般（立位・軽い",
      volume: 26,
      volumeUnit: "kcal",
      duration: 10,
      durationUnit: "min",
    },
  ],
};

const MyDiaries: IDataDiary = {
  diaries: [
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
    {
      time: Date.now(),
      title: "私の日記の記録が一部表示されます。",
      text: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト---テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト",
    },
  ],
};

const getMyExercise = async () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(MyExercises), 2000);
  });
};

const getMyDiary = async () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(MyDiaries), 2000);
  });
};

export { getMyExercise, getMyDiary };
